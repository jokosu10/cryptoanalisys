<?php

class Key {

	/**
	 * Key attribute
	 *
	 * @var String
	 */
	//private $plaintext;
	//private $chippertext;

	/**
	 * Characters collection
	 *
	 * @var array
	 */
	private $chars = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');

	/**
	 * Build key in constructor
	 *
	 * @param [type] $key [description]
	 */
	public function __construct($chippertext, $plaintext)
	{
		$this->chippertext = str_split($chippertext);
		$this->plaintext = str_split($plaintext);
	}

	/**
	 * Encryption formula
	 * E = (p + k) mod 26
	 *
	 * @param  Integer $k index of key text
	 * @param  Integer $p index of plain text
	 * @return Integer
	 */
	private function keyFormula($c, $p)
	{
		$mod = gmp_mod(($c - $p), 26);
		return gmp_strval($mod);
	}

	/**
	 * Search Key process
	 *
	 * @param  String $chippertext plain text
	 * @param  String $plaintext plain text
	 * @return String
	 */
	public function searchKey($chippertext, $plaintext)
	{
		$keyArrayChipper = array();
		$indexChipper    = 0;

		$keyArrayPlain = array();
		$indexPlain    = 0;

		$result   = '';

		// normalization, change to lowercase and remove blank space chippertext
		$lowerChipper   = strtolower($chippertext);
		$trimmedChipper = str_replace(" ", "", $lowerChipper);
		$toArrayChipper = str_split($trimmedChipper);

		// normalization, change to lowercase and remove blank space plaintext
		$lowerPlain   = strtolower($plaintext);
		$trimmedPlain = str_replace(" ", "", $lowerPlain);
		$toArrayPlain = str_split($trimmedPlain);

		// build key array chipper
		for ($i=0; $i < count($toArrayChipper); $i++) {
			if($indexChipper == count($this->chippertext)) {
				$indexChipper = 0;
			}
			$keyArrayChipper[] = $this->chippertext[$indexChipper];
			$indexChipper++;
		}

		// build key array plain
		for ($i=0; $i < count($toArrayPlain); $i++) {
			if($indexPlain == count($this->plaintext)) {
				$indexPlain = 0;
			}
			$keyArrayPlain[] = $this->plaintext[$indexPlain];
			$indexPlain++;
		}

		// calculate search key
		for ($i=0; $i < count($toArrayChipper); $i++) {
			$c = array_search($toArrayChipper[$i], $this->chars);
			$p = array_search($toArrayPlain[$i], $this->chars);

			$keyValue = $this->keyFormula($c, $p);

			$result .= $this->chars[$keyValue];
		}

		return $result;
	}

}
