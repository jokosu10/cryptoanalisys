<?php
$vigenere = require('Vigenere.php');
$key = require('Key.php');

//encryption
$classVigenereEncryption = new Vigenere('dtigasi');
echo $classVigenereEncryption->vigenere('universitas airlangga', true);
echo "\n";

// decryption
$classVigenereDecryption = new Vigenere('dtigasi');
echo $classVigenereDecryption->vigenere('xgqbejalmiyaazotvmgs', false);
echo "\n";

// key
$classKey = new Key('xgqbejalmiyaazotvmgs','universitasairlangga');
echo $classKey->searchKey('xgqbejalmiyaazotvmgs','universitasairlangga');
echo "\n";
